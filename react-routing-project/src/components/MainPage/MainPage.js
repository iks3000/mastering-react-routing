import "./MainPage.scss";
import Logo from '../../images/logo.png';

const MainPage = () => {
    return (
        <section className="premier-block">
            <div className="wrapper">
                <div className="premier-block__wrapp-premier-content">
                    <img className="premier-block__logo" src={Logo} alt="Logo" />
                    <h1 className="main-title">Your Headline <br />Here</h1>
                    <p className="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod.</p>
                </div>
            </div>

        </section>
    )
}

export default MainPage;
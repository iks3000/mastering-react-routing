import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { getUsersByID } from '../../redux/get-users/actions';
import backIcon from '../../images/left-arrow.svg';

import './UserDetails.scss';

const UserDetails = () => {
  const dispatch = useDispatch();
  const { user, error } = useSelector((state) => state.users);

  const { id } = useParams();

  useEffect(() => {
    dispatch(getUsersByID(id));
  }, [dispatch, id]);

  return (
    <>
      {error ? (
        <Redirect to='/not-found' />
      ) : (
        <div className='user'>
          <div className='user__details'>
            <img className='user__img' src={user?.avatar} alt='user' />
            <p className='user__description'>{user?.description}</p>
            <p className='user__name'>
              {user?.firstName} {user?.lastName}
            </p>
            <p className='user__position'> {user?.position} </p>
          </div>
          <Link to='/community' className='not-found__link'>
            <div className='back-icon'>
              <img src={backIcon} alt='back-icon' />
            </div>
            <p>Back to Community page</p>
          </Link>
        </div>
      )}
    </>
  );
};

export default UserDetails;

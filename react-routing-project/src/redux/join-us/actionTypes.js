export const SUBSCRIBE_START = 'SUBSCRIBE_START';
export const SUBSCRIBE_SUCCESS = 'SUBSCRIBE_SUCCESS';
export const SUBSCRIBE_FAILURE = 'SUBSCRIBE_FAILURE';

export const UNSUBSCRIBE_START = 'UNSUBSCRIBE_START';
export const UNSUBSCRIBE_SUCCESS = 'UNSUBSCRIBE_SUCCESS';
export const UNSUBSCRIBE_FAILURE = 'UNSUBSCRIBE_FAILURE';

export const subscribeStart = () => {
    return { type: 'SUBSCRIBE_START' };
};

export const subscribeSuccess = (payload) => {
    return { type: 'SUBSCRIBE_SUCCESS', payload };
};

export const subscribeFailure = (payload) => {
    return { type: 'SUBSCRIBE_FAILURE', payload };
};

export const unSubscribeStart = () => {
    return { type: 'UNSUBSCRIBE_START' };
};

export const unSubscribeSuccess = (payload) => {
    return { type: 'UNSUBSCRIBE_SUCCESS', payload };
};

export const unSubscribeFailure = (payload) => {
    return { type: 'UNSUBSCRIBE_FAILURE', payload };
};